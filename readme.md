Welcome to my Fullstack Tick Tack Toe project. 

Since I have no expertise with PHP, I decided to creeate the backend using Nodejs to help me polish my skills in this new tecnology for me.
In order to comply with the submit date, I decided to create the front end in Angular, which is the framework I currently work faster. 
However, I made it in a very simple way, using mostly Javascript features. This way a person that doesn't know Angular, but knows Javascript, can easily understand the code. Boostrap was used to speed up the styles, and to make the Modal.

I went ahead and used web sockets for the first time. I have always wanted to give it a try, and I though that this was a great opportunity, since we have 2 players sharing information constantly. For this I used the socket.io library.

Since we're fetching data very fast, I decided to use redis to store the data that we're using on the fly.

Conclusion:
I have a great time doing this code challenge. Even if I'm not selected, I still think it was worth doing. Nodejs is not my backend tech that I'm used to working with, but I gained more confidence after this. More importantly, I was able to have fun working around with web sockets and Redis. I hope you liked it and play with the app a bit. Let me know if you have any questions. If I had more time, I would have also done some research on how to implement unit testing for the sockets.

To run this app:

Do npm install on both the root and client folders.
Then node server.js to start the backend
ng serve on the client folder to start the front end.

You can open 2 browser tabs (localhost:4200 by default) to see the interaction between both players. 
Make sure to have redis running. If you have the path added, you would have to run redis-server and then redis-cli.
